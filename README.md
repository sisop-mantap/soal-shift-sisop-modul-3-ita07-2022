# soal-shift-sisop-modul-3-ita07-2022

# Kelompok ITA07
1. Maulanal Fatihil A. M.   5027201031
2. Cherylene Trevina        5027201033
3. M. Hilmi Azis            5027201049

# Daftar Isi
* [Daftar isi](#daftar-isi)
* [Nomor 1](#nomor-1)
* [Penyelesaian Nomor 1](#penyelesaian-nomor-1)
    * [Nomor 1a](#1a)
    * [Nomor 1b](#1b)
    * [Nomor 1c](#1c)
    * [Nomor 1d](#1d)
    * [Nomor 1e](#1e)
* [Nomor 2](#nomor-2)
* [Penyelesaian Nomor 2](#penyelesaian-nomor-2)
    * [Nomor 2a](#2a)
    * [Nomor 2b](#2b)
    * [Nomor 2c](#2c)
    * [Nomor 2d](#2d)
    * [Nomor 2e](#2e)
    * [Nomor 2f](#2f)
    * [Nomor 2g](#2g)
    * [Screenshot output 2](#output-soal-nomor-2)
* [Nomor 3](#nomor-3)
* [Penyelesaian Nomor 3](#penyelesaian-nomor-3)

# Nomor 1
[Daftar isi](#daftar-isi)

Novak mendownload berbagai informasi dari sebuah situs dan terdapat file dengan tulisan tidak jelas dengan format base64. Kami diminta untuk membuat program untuk memecahkan kode-kode tersebut dengan cara decoding base64. Agar lebih cepat, kami disarankan untuk menggunakan thread. <br>
a. Download dua file zip dan unzip file zip tersebut di dua folder yang berbeda dengan nama quote untuk file zip `quote.zip` dan musik untuk file zip `music.zip`. Unzip ini dilakukan dengan bersamaan menggunakan thread. <br>
b. Decode seluruh file `.txt` yang ada dengan base64 dan masukkan hasilnya ke dalam file `.txt` yang baru untuk masing-masing folder (hasilnya ada 2 file `.txt`). Pada saat yang sama dengan menggunakan thread dan dengan nama `quote.txt` dan `music.txt`. Masing-masing kalimat dipisahkan dengan newline/enter. <br>
c. Pindahkan kedua file `.txt` yang berisi hasil decoding ke folder yang baru bernama `hasil`. <br>
d. Folder `hasil` di-zip menjadi file `hasil.zip` dengan password `mihinomenest[namauser]`<br>
e. Karena terdapat hal yang kurang, kami diminta untuk unzip file `hasil.zip` dan buat file `no.txt` dengan tulisan **"No"** pada saat yang bersamaan, lalu zip kedua file hasil dan file `no.txt` menjadi `hasil.zip`, dengan password yang sama seperti sebelumnya. <br>

> Catatan:
> 
> * Boleh menggunakan execv, tetapi tidak menggunakan mkdir/system
> * Jika ada pembenaran soal, akan diupdate di catatan
> * Kedua file.zip berada di folder modul
> * Nama user di passwordnya adalah nama salah satu anggota kelompok, baik yang mengerjakan soal tersebut atau tidak. Misalkan satu kelompok memiliki anggota namanya Yudhistira, Werkudara, dan Janaka. Yang mengerjakan adalah Janaka. Nama passwordnya bisa mihinomenestyudhistira, mihinomenestwerkudara, atau mihinomenestjanaka
> * Menggunakan apa yang sudah dipelajari di Modul 3 dan kalau perlu di Modul 1 dan 2

# Penyelesaian Nomor 1
[Daftar isi](#daftar-isi)

Sebelum menyelesaikan soal nomor 1, kami mendeklarasikan terlebih dahulu library-library yang digunakan pada program kami, yaitu:
```c
#include<stdio.h>
#include<string.h>
#include<pthread.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/wait.h>
#include"base64.h"
```

Untuk file `base64.h` dapat diakses pada [Source Code](soal1/base64.h)

Selanjutnya, kami menggunakan variabel-variabel berikut sebagai variabel global:
```c
#define NUM_DIR 2
#define NUM_GLOBAL 2
#define SIZE 100

pid_t child_id;

pthread_t tid[NUM_DIR];
pthread_t tidGlobal[NUM_GLOBAL]; //inisialisasi array untuk menampung thread dalam kasus ini ada 2 thread

char __create[] = "/bin/mkdir";
char __createT[] = "/usr/bin/touch";
char __download[] = "/bin/wget";
char __unzip[] = "/usr/bin/unzip";
char __decode[] = "/bin/base64";
char __move[] = "/usr/bin/mv";
char __zip[] = "/usr/bin/zip";
char __delete[] = "/usr/bin/rm";

char base46_map[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                     'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                     'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                     'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'};
```

Selanjutnya terdapat fungsi `process` yang akan digunakan untuk memanggil `execv` secara berulang-ulang
```c
void process(char *str, char *argv[]){
    pid_t child_id;
    int status;

    child_id = fork();

    if(child_id == 0){
		execv(str, argv);
	}
    else{
        while (wait(&status) > 0);
    }
}
```

Selanjutnya terdapat function `checkError` yang digunakan untuk mengecek apakah thread berhasil dibuat atau tidak.
```c
void checkError(int err, int i){
    if(err!=0){ //cek error
        printf("\n can't create thread : [%s]", strerror(err));
    }
    else{
        printf("\n create thread [%d] success\n", i + 1);
    }
}
```

Jika pembuatan thread berhasil, maka akan ditampilkan tulisan "create thread [%d] success". Jika gagal maka akan menampilkan "can't create thread : [%s]".

> Penyelesaian untuk soal nomor 1 dapat dilihat pada [Source Code](soal1/soal1.c)

## 1a 
[Daftar isi](#daftar-isi)

Untuk menyelesaikan soal 1a, kami menggunakan source code berikut (pada fungsi **main**)
```c
int err;
/*
|   CREATE FOLDER
*/
for(int i = 0; i<NUM_DIR; i++){ // loop sejumlah thread
    err=pthread_create(&(tid[i]), NULL, &createFolder, NULL); //membuat thread
    checkError(err, i);
}

pthread_join(tid[0],NULL);
pthread_join(tid[1],NULL);

/*
|   UNZIP
*/
for(int i = 0; i<NUM_GLOBAL; i++){ // loop sejumlah thread
    err=pthread_create(&(tidGlobal[i]), NULL, &unZIP, NULL); //membuat thread
    checkError(err, i);
}
pthread_join(tidGlobal[0],NULL);
pthread_join(tidGlobal[1],NULL);
```

- `pthread_create` digunakan untuk membuat thread.
    - Argumen **pertama**, yaitu `tid` dan `tidGlobal` digunakan untuk menunjukkan alamat memori dengan thread ID dari thread baru.
    - Argumen **kedua** digunakan untuk menyesuaikan atribut yang digunakan oleh thread. Karena kami menggunakan `NULL`, maka atribut menggunakan atribut default.
    - Argumen **ketiga**, yaitu thread yang dibuat akan menjalankan fungsi (dalam hal ini adalah `createFolder` dan `unZIP`)
    - Argumen **keempat** digunakan untuk memberikan sebuah argumen ke fungsi pada argumen ketiga. Karena kami tidak menggunakan argumen apapun pada kedua fungsi tersebut, maka kami menggunakan `NULL`.
- Dijalankan fungsi `checkError` untuk mengecek apakah thread berhasil dibuat atau tidak.
- Setelah selesai pembuatan thread, dilakukan `pthread_join` untuk melakukan penggabungan dengan thread lain yang telah berhenti.

Source code yang digunakan untuk fungsi **`createFolder`**:
```c
void* createFolder(void *arg){
    char *directory[NUM_DIR] = {
        "/home/kali/modul3/quote/",
        "/home/kali/modul3/music/"
    };

    char *directoryT[NUM_DIR] = {
        "/home/kali/modul3/quote/quote.txt",
        "/home/kali/modul3/music/music.txt"
    };

	pthread_t id = pthread_self();

    for (int i = 0; i<NUM_DIR; i++){
        char dir[100], tmp[100];
        strcpy(dir, directory[i]);
        strcpy(tmp, directoryT[i]);

        if(pthread_equal(id, tid[i])){ //thread untuk clear layar
            char *argv1[] = {"mkdir", dir, "-p", NULL};
            char *argv2[] = {"touch", tmp, NULL};
            process(__create, argv1);
            process(__createT, argv2);
        }
    }
	return NULL;
}
```

Source code yang digunakan untuk fungsi **`unZIP`**
```c
void* unZIP(void *arg){
    char *directoryUNZIP[NUM_GLOBAL] = {
        "/home/kali/modul3/quote",
        "/home/kali/modul3/music"
    };

    char *folder[NUM_GLOBAL] = {
        "/home/kali/Downloads/quote.zip",
        "/home/kali/Downloads/music.zip"
    };

	pthread_t id = pthread_self();

    for (int i = 0; i<NUM_GLOBAL; i++){
        char dir[100];
        char fold[100];
        strcpy(dir, directoryUNZIP[i]);
        strcpy(fold, folder[i]);

        
        if(pthread_equal(id, tidGlobal[i])){ //thread untuk clear layar
            //
            char *argv1[] = {"unzip", "-q", fold, "-d", dir, NULL};
            process(__unzip, argv1);
        }
    }
	return NULL;
}
```

### Screenshot Ouptut 1a
Pembuatan folder `music` dan `quote`

![Pembuatan folder](img/1a-1.png)

Hasil unzip `quote.zip` dan `music.zip`

![Hasil unzip](img/1a-2.png)

## 1b
[Daftar isi](#daftar-isi)

Untuk men-decode seluruh file .txt yang ada dengan base64 & memasukkan hasilnya ke file .txt baru dalam masing-masing folder, kami menggunakan source code berikut (pada fungsi main):
```c
/*
|   DECODE 
*/
for(int i = 0; i<NUM_GLOBAL-1; i++){ // loop sejumlah thread
    err=pthread_create(&(tidGlobal[i]), NULL, &decodeIsi, NULL); //membuat thread
    checkError(err, i);
}

pthread_join(tidGlobal[0],NULL);
pthread_join(tidGlobal[1],NULL);
```
> Penjelasan untuk pthread sama dengan penjelasan pada [1a](#1a)

Fungsi `decodeIsi`:
```c
void* decodeIsi(void *arg){
    char *directoryDecoded[NUM_GLOBAL] = {
        "/home/kali/modul3/quote/",
        "/home/kali/modul3/music/"
    };    

    char *destination[NUM_GLOBAL] = {
        "/home/kali/modul3/quote/quote.txt",
        "/home/kali/modul3/music/music.txt"
    }; 

    char *adder[NUM_GLOBAL] = {
        "q",
        "m"
    };

    pthread_t id = pthread_self();

    for (int i = 0; i<NUM_GLOBAL; i++){
        char dir[100];
        char add[100];
        char temp[100];
        char str[100], value[100];
        
        if(pthread_equal(id, tidGlobal[i])){
            for(int j = 1; j<10; j++){
                strcpy(dir, directoryDecoded[i]);
                strcpy(add, adder[i]);
                strcat(dir, add);
                sprintf(temp, "%d.txt", j);
                strcat(dir, temp);

                printf("hasil dir: %s \n", dir);

                
                FILE *file = fopen(dir, "r"), *dest;
                if(file == NULL){
                    fclose(file);
                    printf("Error! opening file");
                }
                else{
                    printf("Sudah terbuka \n\n");
                }

                
                while (fgets(str, 100, file) != NULL) {
                    printf("%s \n", str);
                    
                    unsigned char *value = base64_decode(str);
                    printf("%s \n", value);

                    dest = fopen(destination[i], "a");
                    fprintf(dest, "%s\n", value);
                }
                fclose(dest);
                fclose(file);
                
                memset(dir, 0, sizeof(dir));
                memset(value, 0, sizeof(value));
            }
        }
    }
}
```

Source code untuk fungsi `base64_decode`:
```c
char* base64_decode(char* cipher) {
    char counts = 0;
    char buffer[4];
    char* plain = malloc(strlen(cipher) * 3 / 4);
    int i = 0, p = 0;

    for(i = 0; cipher[i] != '\0'; i++) {
        char k;
        for(k = 0 ; k < 64 && base46_map[k] != cipher[i]; k++);
        buffer[counts++] = k;
        if(counts == 4) {
            plain[p++] = (buffer[0] << 2) + (buffer[1] >> 4);
            if(buffer[2] != 64)
                plain[p++] = (buffer[1] << 4) + (buffer[2] >> 2);
            if(buffer[3] != 64)
                plain[p++] = (buffer[2] << 6) + buffer[3];
            counts = 0;
        }
    }

    plain[p] = '\0';    /* string padding character */
    return plain;
}
```

### Screenshot Output 1b
File `music.txt` & `quote.txt` **sebelum** program decode dijalankan:

![Sebelum decode](img/1b-1.png)

File `music.txt` & `quote.txt` **sesudah** program decode dijalankan:

![Sesudah decode](img/1b-2.png)

## 1c
[Daftar isi](#daftar-isi)

Untuk memindahkan file .txt yang telah di-decode ke folder hasil, kami menggunakan source code berikut (pada fungsi main):
```c
/*
|   MOVE
*/
for(int i = 0; i<NUM_GLOBAL; i++){ // loop sejumlah thread
    err=pthread_create(&(tidGlobal[i]), NULL, &moveFile, NULL); //membuat thread
    checkError(err, i);
}

pthread_join(tidGlobal[0],NULL);
pthread_join(tidGlobal[1],NULL);
```
> Penjelasan untuk pthread sama dengan penjelasan pada [1a](#1a)

Berikut adalah source code untuk fungsi `moveFile`

```c
void* moveFile(void *arg){
    char *directoryMove[NUM_GLOBAL] = {
        "/home/kali/modul3/quote/quote.txt",
        "/home/kali/modul3/music/music.txt"
    };

    char folder[] = "/home/kali/modul3/hasil";

    FILE *file;
    if(file = fopen(folder, "r")){
        fclose(file);
        printf("Folder sudah ada \n");
    }
    else{
        printf("Folder belum ada \n");

        char *argv1[] = {"mkdir", folder, "-p", NULL};
        process(__create, argv1);
    }

    sleep(2);

	pthread_t id = pthread_self();

    for (int i = 0; i<NUM_GLOBAL; i++){
        char dir[100];
        strcpy(dir, directoryMove[i]);
        printf("%s \n", dir);
        
        if(pthread_equal(id, tidGlobal[i])){ //thread untuk clear layar
            char *argv1[] = {"mv", dir, folder, NULL};
            process(__move, argv1);
        }

        memset(dir, 0, sizeof(dir));
    }
}
```

* Sebelum melakukan pembuatan folder `hasil`, dicek terlebih dahulu apakah folder `hasil` sudah ada atau belum. Jika sudah ada, maka akan muncul tulisan "Folder sudah ada" dan dilanjutkan ke proses berikutnya. Jika belum ada maka akan muncul tulisan "Folder belum ada" dan langsung dilakukan pembuatan folder.
* Setelah dilakukan pembuatan folder, file-file yang sudah di-decode dipindahkan ke dalam folder `hasil`

### Screenshot Output 1c
Isi folder `hasil`

![folder hasil](img/1c-1.png)

Isi Folder `music`

![folder music](img/1c-2.png)

Isi Folder `quote`

![folder quote](img/1c-3.png)

## 1d
[Daftar isi](#daftar-isi)

Selanjutnya, folder `hasil` di-zip menjadi `hasil.zip` menggunakan source code berikut (pada fungsi main):
```c
/*
|   ZIP
*/
chdir("/home/kali/modul3/hasil");
for(int i = 0; i<NUM_GLOBAL-1; i++){ // loop sejumlah thread
err=pthread_create(&(tidGlobal[i]), NULL, &ziip, NULL); //membuat thread
    checkError(err, i);
}
pthread_join(tidGlobal[0],NULL);
pthread_join(tidGlobal[1],NULL);
```
> Penjelasan untuk pthread sama dengan penjelasan pada [1a](#1a)

Source code untuk fungsi `ziip` yang digunakan untuk membuat zip:
```c
void* ziip(void *arg){
    char *argv1[] = {"zip", "--encrypt", "hasil.zip", "music.txt", "quote.txt", NULL};
    process(__zip, argv1);
    sleep(2);
    char *argv2[] = {"mv", "hasil.zip", "/home/kali/modul3", NULL};
    process(__move, argv2);
}
```
> kami kesulitan untuk memasukkan password untuk zip pada codingan sehingga kami memasukkan password secara manual pada terminal ketika program telah dijalankan, setelah itu dilakukan pemindahan terhadap file-file tersebut ke dalam `hasil.zip`.

Setelah melakukan zip folder "hasil.zip"

![hasil.zip](img/1d-1.png)

Screenshot untuk memasukkan password **mihinomenestchery** pada terminal (karena kesulitan untuk memasukkan password pada source code)

![password](img/1d-2.png)

Screenshot isi dari hasil.zip, yang harus dibuka dengan password yang telah ditentukan.

![hasil.zip](img/1d-3.png)

Screenshot music.txt (yang dibuka dengan password telah ditentukan)

![music.txt](img/1d-4.png)

## 1e
[Daftar isi](#daftar-isi)

Pada soal 1e, kami diminta untuk mengunzip `hasil.zip` dan membuat file `no.txt` yang berisikan tulisan "No" pada saat yang bersamaan. Setelah itu, kami diminta untuk menggabungkan hasil unzip dengan file `no.txt` menjadi zip `hasil.zip` kembali dengan password yang sama. Source code yang digunakan (pada fungsi main):
```c
/*
|   GASS
*/ 
chdir("/home/kali/modul3");
for(int i = 0; i<NUM_GLOBAL-1; i++){ // loop sejumlah thread
    err=pthread_create(&(tidGlobal[i]), NULL, &gass, NULL); //membuat thread
    checkError(err, i);
}

pthread_join(tidGlobal[0],NULL);
pthread_join(tidGlobal[1],NULL);
```

Source code untuk fungsi `gass`:
```c
void* gass(void *arg){
    char *argv1[] = {"unzip", "-q", "hasil.zip", NULL};
    process(__unzip, argv1);

    //cek ada atau tidak file no.txt
    char folder[] = "no.txt";
    FILE *file, *dest;
    if(file = fopen(folder, "r")){
        fclose(file);
        printf("File sudah ada \n");
    }
    else{
        printf("File belum ada \n");

        char *argv1[] = {"touch", folder, NULL};
        process(__createT, argv1);
    }

    sleep(2);

    //buka dan tulis
    dest = fopen(folder, "a");
    fprintf(dest, "No");
    fclose(dest);
    
    sleep(2);
    char *argv3[] = {"zip", "--encrypt", "hasil.zip", "music.txt", "quote.txt", "no.txt",NULL};
    process(__zip, argv3);

    char *argv4[] = {"rm", "-r", "hasil", "music.txt", "quote.txt", "no.txt",NULL};
    process(__delete, argv4);
}
```

### Screenshot Output 1e
Isi dari `hasil.zip` setelah seluruh program dijalankan:

![hasil.zip](img/1e-1.png)

Isi dari file `no.txt`, `quote.txt`, dan `music.txt` setelah memasukkan password yang telah ditentukan

# Nomor 2
[Daftar isi](#daftar-isi)

Bluemary adalah seorang Top Global 1 di salah satu platform online judge. Suatu hari Ia ingin membuat online judge nya sendiri, namun dikarenakan Ia sibuk untuk mempertahankan top global nya, maka Ia meminta kamu untuk membantunya dalam membuat online judge sederhana. Online judge sederhana akan dibuat dengan sistem client-server dengan beberapa kriteria sebagai berikut:

a.	Pada saat client terhubung ke server, terdapat dua pilihan pertama yaitu register dan login. Jika memilih register, client akan diminta input id dan passwordnya untuk dikirimkan ke server. Data input akan disimpan ke file 'users.txt' dengan format **username:password.** Jika client memilih login, server juga akan meminta client untuk input id dan passwordnya lalu server akan mencari data di users.txt yang sesuai dengan input client. Jika data yang sesuai ditemukan, maka client dapat login dan dapat menggunakan command-command yang ada pada sistem. Jika tidak maka server akan menolak login client. Username dan password memiliki kriteria sebagai berikut:
- Username unique (tidak boleh ada user yang memiliki username yang sama)
- Password minimal terdiri dari 6 huruf, terdapat angka, terdapat huruf besar dan kecil

b.	Sistem memiliki sebuah database pada server untuk menampung problem atau soal-soal yang ada pada online judge. Database ini bernama problems.tsv yang terdiri dari **judul problem dan author problem (berupa username dari author), yang dipisah dengan \t.** File otomatis dibuat saat server dijalankan.

c.	Client yang telah login, dapat memasukkan command yaitu 'add' yang berfungsi untuk menambahkan problem/soal baru pada sistem. Saat client menginputkan command tersebut, server akan meminta beberapa input yaitu:
- Judul problem (unique, tidak boleh ada yang sama dengan problem lain)
- Path file description.txt pada client (file ini berisi deskripsi atau penjelasan problem)
- Path file input.txt pada client (file ini berguna sebagai input testcase untuk menyelesaikan problem)
- Path file output.txt pada client (file ini berguna untuk melakukan pengecekan pada submission client terhadap problem)

d.  **Client yang telah login,** dapat memasukkan command 'see' yang berguna untuk menampilkan seluruh judul problem yang ada beserta authornya(author merupakan username client yang menambahkan problem tersebut). 

e.  **Client yang telah login,** dapat memasukkan command 'download <judul-problem>' yang berguna untuk mendownload file description.txt dan input.txt yang berada pada folder pada server dengan nama yang sesuai dengan argumen kedua pada command yaitu <judul-problem>. Kedua file tersebut akan disimpan ke folder dengan nama <judul-problem> di client.

f.	**Client yang telah login,** dapat memasukkan command 'submit <judul-problem> <path-file-output.txt>.'  Command ini berguna untuk melakukan submit jawaban dari client terhadap problem tertentu. Algoritma yang dijalankan adalah client akan mengirimkan file output.txt nya melalui argumen ke 3 pada command, lalu server akan menerima dan membandingkan isi file output.txt yang telah dikirimkan oleh client dan output.txt yang ada pada folder dengan nama yang sesuai dengan argumen ke 2 pada command. Jika file yang dibandingkan sama, maka server akan mengirimkan pesan “AC” dan jika tidak maka server akan mengeluarkan pesan “WA”.

g.	Server dapat menangani multiple-connection. Dimana jika terdapat 2 atau lebih client yang terhubung ke server, maka harus menunggu sampai client pertama keluar untuk bisa melakukan login dan mengakses aplikasinya.

**Note:** 
<ul>
<li>Dilarang menggunakan system() dan execv(). Silahkan dikerjakan sepenuhnya dengan thread dan socket programming. 
<li>Untuk download dan upload silahkan menggunakan file teks dengan ekstensi dan isi bebas (yang ada isinya bukan touch saja dan tidak kosong)
<li>Untuk error handling jika tidak diminta di soal tidak perlu diberi.
</ul>

# Penyelesaian Nomor 2
[Daftar isi](#daftar-isi)


Berikut adalah library yang kami gunakan, baik pada server maupun pada client
```c
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
```
## 2a 
[Daftar isi](#daftar-isi)

**Client**

Pertama - tama Client akan mencoba untuk melakukan koneksi dengan server menggunakan socket. Terdapat beberapa kasus yang akan menyebabkan kegagalan dalam pengubungan server dan client yang akan mengembalikan pesan error seperti kegagalan dalam pembuatan socket yang akan mengembalikan "Failed to create socket", kegagalan dalam pencarian alamat address yang akan mengembalikan "Invalid address", dan kegagalan penyambungan koneksi yang akan mengembalikan "Connection failed".

```c
 struct sockaddr_in alamatServer;
  char *login = "login", *logout = "logout", *regist = "register";
  char temp, buffer[1100] = {0}, cmd[1100], username[1100], password[1100];
  if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
  {
    printf("Failed to create socket\n");
    return -1;
  }
  memset(&alamatServer, '0', sizeof(alamatServer));
  alamatServer.sin_family = AF_INET, alamatServer.sin_port = htons(port);
  if (inet_pton(AF_INET, "127.0.0.1", &alamatServer.sin_addr) <= 0)
  {
    printf("Invalid address\n");
    return -1;
  }
  if (connect(sock, (struct sockaddr *)&alamatServer, sizeof(alamatServer)) < 0)
  {
    printf("Connection failed\n");
    return -1;
  }
  readerConn = read(sock, buffer, 1100);
  printf("%s\n", buffer);
  while (!same("Connected", buffer))
  {
    printf("Enter anything to check whether you can connect to the server or not.\n");
    scanf("%s", cmd);
    send(sock, cmd, 1100, 0);
    readerConn = read(sock, buffer, 1100);
    printf("%s\n", buffer);
  }
  if (!same("Connected", buffer))
    return 0;
```
Setelah koneksi antara Client dan Server, akan muncul pilihan untuk Client melakukan login, register, atau exit. Fungsi scanf akan mengambil command dari client dan menjalankan fungsi selanjutnya sesuai dengan command yang diberikan oleh client.

```c
    while (1)
  {
    printf("1. Login\n2. Register\n3. Exit\n");
    scanf("%s", cmd);
    if (same(cmd, login))
    {
```
Jika client memasukkan command login, maka akan muncul pesan agar client memasukkan username dan password yang akan dikirimkan kepada server. Jika dikembalikan "LoginSuccess" dari server, client akan dapat masuk ke tahap berikutnya dan pesan bahwa login berhasil akan ditampilkan. Server akan membaca file pada user.txt dan melakukan compare terhadap informasi login yang telah diberikan oleh user untuk menentukan apakah user berhasil login atau tidak.

```c
     while (1)
  {
    if (same(cmd, login))
    {
      send(sock, login, strlen(login), 0);
      printf("Username: ");
      scanf("%c", &temp);
      scanf("%[^\n]", username);
      send(sock, username, 1100, 0);
      printf("Password: ");
      scanf("%c", &temp);
      scanf("%[^\n]", password);
      send(sock, password, 1100, 0);
      memset(buffer, 0, sizeof(buffer));
      readerConn = read(sock, buffer, 1100);
      if (same(buffer, "LoginSuccess"))
        printf("Login process is success! Welcome aboard! ^o^\n");
      else
        printf("Login process failed, please try again.\n");
```

```c
    while (1)
  {
    readerConn = read(serverSocket, buffer, 1100);
    if (same(login, buffer))
    {
      readerConn = read(serverSocket, user.name, 1100);
      readerConn = read(serverSocket, user.password, 1100);
      fp3 = fopen("users.txt", "r");
      int check = 0;
      char *line = NULL;
      ssize_t len = 0;
      ssize_t fileReader;
      while ((fileReader = getline(&line, &len, fp3) != -1))
      {
        char userName[105], userPass[105];
        int i = 0, j = 0;
        while (line[i] != ':')
          userName[i] = line[i], i++;
        userName[i++] = '\0';
        while (line[i] != '\n')
          userPass[j] = line[i], j++, i++;
        userPass[j] = '\0';
        if (strcmp(user.name, userName) == 0 && strcmp(user.password, userPass) == 0)
        {
          check = 1, send(serverSocket, "LoginSuccess", strlen("LoginSuccess"), 0);
          break;
        }
      }
```
## 2b
[Daftar isi](#daftar-isi)

Setelah server berhasil dijalankan, akan dieksekusi fungsi fopen mode "a" yang akan membuat file problems.tsv jika sebelumnya file tersebut belum ada.

```c
int main(int argc, char const *argv[])
{
  struct sockaddr_in alamat;
  int serverSocket, serverFD, readerConn, opt = 1, jumlahKoneksi = 5, porter = 8080, addr_len = sizeof(alamat);
  if ((serverFD = socket(AF_INET, SOCK_STREAM, 0)) == 0)
    perror("socket failed"), exit(EXIT_FAILURE);
  if (setsockopt(serverFD, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt)))
    perror("setsockopt"), exit(EXIT_FAILURE);
  alamat.sin_family = AF_INET, alamat.sin_addr.s_addr = INADDR_ANY, alamat.sin_port = htons(porter);
  if (bind(serverFD, (struct sockaddr *)&alamat, sizeof(alamat)) < 0)
    perror("bind failed"), exit(EXIT_FAILURE);
  if (listen(serverFD, jumlahKoneksi) < 0)
    perror("listen"), exit(EXIT_FAILURE);
  mkdir("problems", 0777);
  FILE *fp = fopen("problems.tsv", "a");
```
## 2c
[Daftar isi](#daftar-isi)

Client berhasil melakukan login, client akan mendapatkan beberapa pilihan. Salah satunya adalah untuk melakukan add problem.
Kemudian client memasukkan command add, akan muncul pesan untuk memasukkan judul problem yang akan diambil menggunakan fungsi scanf dan dikirimkan kepada server. Akan muncul juga pesan untuk memasukkan filepath description.txt yang berisi deskripsi problem, 
```c
if (same(buffer, "LoginSuccess"))
      {
        while (1)
        {
          printf("1. Logout\n2. Add\n3. Download [Problem Name]\n4. Submit [Problem Name] [Path Output]\n5. See\n");
          scanf("%s", cmd);
          if (same(cmd, logout))
          {
            send(sock, logout, 1100, 0);
            break;
          }
```

```c
else if (same(cmd, "add"))
          {
            send(sock, cmd, 1100, 0);
            char data[1100], problemName[1100];
            printf("Judul Problem: ");
            scanf("\n%[^\n]%*c", problemName);
            send(sock, problemName, 1100, 0);
            printf("Filepath description.txt: ");
            scanf("\n%[^\n]%*c", data);
            char outputFolder[1100], inputFolder[1100], descFolder[1100], outputPath[1100];
            strcpy(descFolder, data);
            removeStr(descFolder, "/description.txt");
            mkdir(descFolder, 0777);
            send(sock, data, 1100, 0);
            printf("Filepath input.txt: ");
            scanf("\n%[^\n]%*c", data);
            strcpy(inputFolder, data);
            removeStr(inputFolder, "/input.txt");
            mkdir(inputFolder, 0777);
            send(sock, data, 1100, 0);
            printf("Filepath output.txt: ");
            scanf("\n%[^\n]%*c", data);
            send(sock, data, 1100, 0);
            readerConn = read(sock, buffer, 1100);
            int numGacha = atoi(buffer);
            memset(buffer, 0, sizeof(buffer));
            strcpy(outputPath, data);
            FILE *fileManager;
            fileManager = fopen(outputPath, "a+");
            for (int i = 0; i < numGacha; i++)
              readerConn = read(sock, buffer, 1100),
              fprintf(fileManager, "%s", buffer),
              memset(buffer, 0, sizeof(buffer));
            fclose(fileManager);
            printf("Successfully adding problem %s to the server!\n", problemName);
            continue;
          }
    ```

## 2d
[Daftar isi](#daftar-isi)

```c
if (same(buffer, "LoginSuccess"))
      {
        while (1)
        {
          printf("1. Logout\n2. Add\n3. Download [Problem Name]\n4. Submit [Problem Name] [Path Output]\n5. See\n");
          scanf("%s", cmd);
          if (same(cmd, logout))
          {
            send(sock, logout, 1100, 0);
            break;
          }
```

```c
else if (same(cmd, "see"))
          {
            printf("Problems Available in Online Judge:\n");
            send(sock, cmd, 1100, 0);
            memset(buffer, 0, sizeof(buffer));
            while (readerConn = read(sock, buffer, 1100))
            {
              if (same(buffer, "e"))
                break;
              printf("%s", buffer);
            }
            continue;
```
server
```c
else if (same("see", buffer))
          {
            fp = fopen("problems.tsv", "r");
            if (!fp)
            {
              send(serverSocket, "e", sizeof("e"), 0);
              memset(buffer, 0, sizeof(buffer));
              continue;
            }
            char *line = NULL;
            ssize_t len = 0;
            ssize_t fileReader;
            while ((fileReader = getline(&line, &len, fp) != -1))
            {
              problem temp_problem;
              storeProblemTSV(&temp_problem, line);
              char message[1100];
              sprintf(message, "%s by %s", temp_problem.problem_title, temp_problem.author);
              send(serverSocket, message, 1100, 0);
            }
            send(serverSocket, "e", sizeof("e"), 0), fclose(fp);
            memset(buffer, 0, sizeof(buffer));
          }
        }

```
## 2e
[Daftar isi](#daftar-isi)

```c
if (same(buffer, "LoginSuccess"))
      {
        while (1)
        {
          printf("1. Logout\n2. Add\n3. Download [Problem Name]\n4. Submit [Problem Name] [Path Output]\n5. See\n");
          scanf("%s", cmd);
          if (same(cmd, logout))
          {
            send(sock, logout, 1100, 0);
            break;
          }
```
## 2f
[Daftar isi](#daftar-isi)

```c
else if (same(cmd, "submit"))
          {
            send(sock, cmd, 1100, 0);
            memset(buffer, 0, sizeof(buffer));
            char namaProblem[1100], filePath[1100];
            scanf("%s %s", namaProblem, filePath);
            send(sock, namaProblem, 1100, 0);
            FILE *fpC = fopen(filePath, "r");
            if (!fpC)
            {
              printf("Output file is not exist!\n");
              break;
            }
            char *line = NULL;
            ssize_t len = 0;
            ssize_t fileReader;
            bool checkSubmission = true;
            while ((fileReader = getline(&line, &len, fpC) != -1))
            {
              char outputSubmission[105];
              int i = 0, j = 0;
              while (line[i] != ':')
                i++;
              line[i++] = '\0';
              while (line[i] != '\n')
                outputSubmission[j++] = line[i++];
              outputSubmission[j] = '\0';
              send(sock, outputSubmission, 105, 0);
            }
            memset(buffer, 0, sizeof(buffer));
            readerConn = read(sock, buffer, 1100);
            printf("Your output.txt file for problem %s is submitted!\nResult: %s\n", namaProblem, buffer);
          }
```
## 2g
[Daftar isi](#daftar-isi)

```c
char buffer[1100] = {0}, connected[1100] = "Connected", failed[1100] = "Other people are connected, please wait for them ^o^";
  int readerConn, serverSocket = *(int *)tmp;
  if (total == 1)
    send(serverSocket, connected, 1100, 0);
  else
    send(serverSocket, failed, 1100, 0);
  while (total > 1)
  {
    readerConn = read(serverSocket, buffer, 1100);
    if (total == 1)
      send(serverSocket, connected, 1100, 0);
    else
      send(serverSocket, failed, 1100, 0);
  }
```
## Output soal nomor 2
2a register

![output 2a](img/2a.jpg)

2a login

![output 2a-1](img/2a-1.jpg)

2b problems.tsv

![output 2b](img/2b.jpg)

2c add

![add](img/2c.jpg)

2d see

![see](img/2d.jpg)

2e download

![download](img/2e.jpg)

2g menunggu client 

![nunggu client lain](img/2g.jpg)
# Nomor 3
[Daftar isi](#daftar-isi)

Nami adalah seorang pengoleksi harta karun handal. Karena Nami memiliki waktu luang, Nami pun mencoba merapikan harta karun yang dimilikinya berdasarkan jenis/tipe/kategori/ekstensi harta karunnya. Setelah harta karunnya berhasil dikategorikan, Nami pun mengirimkan harta karun tersebut ke kampung halamannya.

# Penyelesaian Nomor 3
[Daftar isi](#daftar-isi)

untuk membuat list file secara rekrusif, caranya adalah menggunakan function reksv dibawah ini
```
void reksv(char *disini){
    char path[PATH_MAX];
    struct dirent *dp;
    struct stat buffer;
    DIR *dir = opendir(disini);
    if(!dir) return;
    while((dp = readdir(dir)) != NULL){
        if(strcmp(dp->d_name, ".") && strcmp(dp->d_name, "..")){
            char buffer1[200];
            int buffer_len1 = sizeof(buffer1);
            snprintf(buffer1, buffer_len1, "%s/%s", disini, dp->d_name);
            strcpy(path, buffer1);
            if(!stat(path, &buffer) && S_ISREG(buffer.st_mode)){
                pthread_t thread;
                int err = pthread_create(&thread, NULL, eks, (void *)path);
                pthread_join(thread, NULL);
            }
            reksv(path);
        }
    } closedir(dir);
}
```

## 3. mengkategorikan file dan memindahkan ke foldernya
Dari file-file yang ada, akan dilakukan pengecekan terlebih dahulu dengan cara menggunakan function cek untuk mengetahui apakah file tersebut hidden atau visible
```
int cek(const char *namaF){
    struct stat banding;
    if(!stat(namaF, &banding)) return 1;
    return 0;
}
```

Kemudian juga ada ada function eks, yang mana pada function ini akan membuat folder sesuai dengan nama dari ekstensinya. yang nantinya akan memindahkan file-file tersebut ke dalam folder yang mempunyai ekstensi yang sama

Selain itu juga akan dibuat folder hidden yang berguna untuk menyimpan file yang tersembunyi, pun juga ada folder unknown yang dibuat untuk file yang eksensinya tidak ada.

untuk menentukan nama dari ekstensinya, menggunakan cara
```
  strtok(file, ".");
```
yang mana akan dibatasi/memisahkan namafile dari .

berikutnya, dengan menggunakan function strLwr pada hasil ekstensi yang didapat bertujuan mengubah file yang ekstensinya dalam huruf kapital menjadi huruf kecil sehingga bisa digabungkan ke folder yang sama
```
  strLwr(hasil);
```
lalu akan dibuat foldernya sesuai dengan ekstensi yang telah didapat dan memindahkan file ke lokasi yang dituju menggunakan fungsi C rename()

```
void *eks(void *namaF){
    char visible[200], dirname[200], cwd[PATH_MAX], hd_f[200], file[200], hd[200];
    strcpy(visible, namaF);
    strcpy(hd_f, namaF);
    char *ketemu = strrchr(hd_f, '/');
    strcpy(hd, ketemu);
    if(hd[1] == '.'){
        strcpy(dirname, "hartakarun/");
        strcat(dirname, "Hidden");
    }else if (strstr(namaF, ".") != NULL){
        strcpy(file, namaF);
        strtok(file, ".");
        
        char hasil[200], simpan[200];
        strcpy(hasil, "");
        strcpy(simpan, "");

        int a = strlen(visible) - 1;
       
        int ok = 0;
       
        while(a >= 0){

            if(visible[a] == '.'){
       
                if(!ok){
                    ok = 1;
                 strLwr(hasil);
                    strcpy(simpan, hasil);
                }else{
                
                    strcpy(simpan, hasil);
                    break;
                }
            }else if(visible[a] == '/') break;
            char buffer1[200];
            int buffer_len1 = sizeof(buffer1);
            snprintf(buffer1, buffer_len1, "%s%c", hasil, visible[a]);
            strcpy(hasil, buffer1);
            a--;
        }

        int lastt = strlen(simpan) - 1;
        char answer_path[200] = "";
        for(; lastt >= 0; lastt--){
            char buffer1[200];
            int buffer_len1 = sizeof(buffer1);
            snprintf(buffer1, buffer_len1, "%c", simpan[lastt]);
            strcat(answer_path, buffer1);
        }
        strcpy(dirname, "hartakarun/");
        strcat(dirname, answer_path);
      
    }else{
        strcpy(dirname, "hartakarun/");
        strcat(dirname, "Unknown");
    }

    int ada = cek(visible);

    if(!strcmp(dirname, "hartakarun/_")) 
    strcpy(dirname, "hartakarun/__");
    if(ada){
    
        mkdir(dirname, 0777);

    }
    else puts("error");
    if(getcwd(cwd, sizeof(cwd)) != NULL){
        char *nama = strrchr(namaF, '/');
        char namafile[200];
        strcpy(namafile, cwd);
        strcat(namafile, "/");
        strcat(namafile, dirname);
        strcat(namafile, nama);

        rename(namaF, namafile);
    }
}
```

## 3. client
pada bagian client 
digunakan pid_t untuk melakukan zip folder harta karun dan disimpan ke directory client yang nantinya akan dikirim ke folder server

  ```
   pid_t zip;
    zip = fork();
    if(zip < 0){
        exit(EXIT_FAILURE);
    }
    if(!zip) execl("/usr/bin/zip", "zip", "-r", "hartakarun", "../hartakarun", "-q", NULL);
```

  ## 3. server  
  pada bagian server
  Soal meminta apabila pada bagian client menginputkan pesan"send hartakarun.zip" maka file zip itu akan dikirimkan ke folder server
  disini juga menggunakan strcmp yang berfungsi untuk membandingkan apakah yang telah diinputkan di client (disimpan di buffer) berisikan send hartakarun.zip, dan apabila sama maka akan melakukan return 0. <br>

  kemudian akan dilanjutkan ke proses pemindahan dari dari folder client ke server
  ```
  int hasil = strcmp(buffer,"send hartakarun.zip");
    if(hasil==0){
        char cwd[PATH_MAX];
        getcwd(cwd, sizeof(cwd));
        int apaini = 0;
        while(cwd[apaini] != '\0') apaini++;
        char ini_path[200];
        strncpy (ini_path, cwd, apaini - 7);
        char goalpath[200], startpath[200];
        strcpy(startpath, ini_path);
        strcpy(goalpath, ini_path);
        strcat(startpath, "/Client/hartakarun.zip");
        strcat(goalpath, "/Server/hartakarun.zip");
        rename(startpath, goalpath);
    }
  ```


## Output soal 3
folder hartakarun
![output 3.2](/img/3.2.png)

folder ekstensi yang berisi file yang sesuai
![output 3.1](/img/3.1.png)

salah satu contoh folder yang berisikan file berekstensi JPG dan jpg
![output 3.5](/img/3.5.png)

hasil zip dari client
![output 3.3](/img/3.3.png)

hasil pengiriman file ke server
![output 3.3](/img/3.3.png)